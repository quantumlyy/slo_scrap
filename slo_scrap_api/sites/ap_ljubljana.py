from typing import Dict, List

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import ap_ljubljana
from slo_scrap_lib.sites.ap_ljubljana import Vožnja

router = APIRouter(
    prefix="/ap_ljubljana",
    tags=["AP Ljubljana"],
)

@router.get(
    "/postaje",
    description="Tabela postaj {id → ime}"
)
@cache(expire=24*60*60)
def postaje() -> Dict[str, str]:
    return ap_ljubljana.seznam_postaj()

@router.get(
    "/vozni_red/{vstop_id}/{izstop_id}",
    response_model=List[Vožnja],
    description="Vožnje od→do na dan datum (format D.M.YYYY)"
)
@cache(expire=24*60*60)
def vozni_red(vstop_id: int, izstop_id: int, slo_datum: str) -> List[Vožnja]:
    return ap_ljubljana.vozni_red(vstop_id, izstop_id, slo_datum)