import io

from captcha_solver import CaptchaSolver
from fastapi import APIRouter, Query
from fastapi_cache.decorator import cache
from starlette.responses import StreamingResponse

from slo_scrap_api import secrets
from slo_scrap_lib.sites.esodstvo import TipNepremicnine, eVlozisce

router = APIRouter(
    prefix="/esodstvo",
    tags=["e-Sodstvo"],
)

v: eVlozisce = None

def init_if_needed():
    global v
    if v:
        return
    v = eVlozisce(session_file="../session.pickle", captcha_solver=CaptchaSolver("2captcha", api_key=secrets.TWOCAPTCHA_API_KEY))
    r1 = v.login(secrets.EVLOZISCE.EMAIL, secrets.EVLOZISCE.PASSWORD)

    r2 = v.role_eopravila_reg()

    r3 = v.izberi_ezk()


@router.get(
    "/ezk_izpis",
    responses={
        200: {
            "content": {"application/pdf": {}},
            "description": "PDF result",
        }
    },
    description="PDF izpiz iz e-Zemljiške Knjige. Deluje le med 8:00 in 20:00 (omejitev e-Vložišča). 24h cache"
)
@cache(expire=24*60*60)
def ezk_izpis(tip_nepremicnine: str = Query("PARCELA", enum=[tip.name for tip in TipNepremicnine]), katastrska_obcina_št: int = None, številka_parcele: str = "") -> StreamingResponse:
    init_if_needed()
    tip_nepremicnine = getattr(TipNepremicnine, tip_nepremicnine)
    args = (
        tip_nepremicnine,
    )
    kwargs = dict(
        katastrska_občina=katastrska_obcina_št,
        številka_parcele=številka_parcele,
    )
    resp = v.ezk_izpis(*args, **kwargs)
    response = StreamingResponse(io.BytesIO(resp.content), media_type="application/pdf")
    response.headers["Content-Disposition"] = resp.headers["Content-Disposition"]
    return response


