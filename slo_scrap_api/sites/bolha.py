from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from slo_scrap_lib.sites import bolha
from slo_scrap_lib.sites.bolha import Oglas

router = APIRouter(
    prefix="/bolha",
    tags=["Bolha.com"],
)

@router.get(
    "/oglas/{oglas_id}",
    response_model=Oglas,
    description="Podatki o enem oglasu."
)
def bolha_single(oglas_id: int):
    oglas = bolha.read_ad(ad_id=oglas_id)
    json = jsonable_encoder(oglas, exclude_none=True)
    return JSONResponse(json)
