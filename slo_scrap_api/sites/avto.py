from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import bolha, avto
from slo_scrap_lib.sites.avto import Avto
from slo_scrap_lib.sites.bolha import Oglas

router = APIRouter(
    prefix="/avto_net",
    tags=["Avto.net"],
)

@router.get(
    "/avto/{avto_id}",
    response_model=Avto,
    description="Podatki o enem oglasu"
)
@cache(expire=60*60)
def avto_single(avto_id: int) -> Avto:
    return avto.parse_single(id=avto_id)