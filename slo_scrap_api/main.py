
import logging

import uvicorn
from fastapi import FastAPI
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend

from slo_scrap_api.sites import bolha, avto, esodstvo, ap_ljubljana

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("rest_api")

app = FastAPI(
    title="Razni scraped APIji",
    description="Improviziran API za razne (večinoma slovenske) strani ki nimajo svojih APIjev."
)

@app.on_event("startup")
async def on_startup():
    FastAPICache.init(InMemoryBackend())

app.include_router(bolha.router)
app.include_router(avto.router)
app.include_router(esodstvo.router)
app.include_router(ap_ljubljana.router)


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5000, log_level="info")
