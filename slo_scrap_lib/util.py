from itertools import tee
from typing import Iterable, Iterator


def flatten(lst_lst: Iterable[Iterable]) -> Iterable:
    for lst in lst_lst:
        for el in lst:
            yield lst


def pairwise(iterable: Iterable):
    prev = None
    for itm in iterable:
        if prev:
            yield prev, itm
            prev = None
        else:
            prev = itm
