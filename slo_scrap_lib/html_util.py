from typing import List, Dict

from bs4 import Tag
from lxml.html import HtmlElement


def textify(el: Tag) -> str:
    return el.text.strip()

def html_list_to_list(html_list) -> List[str]:
    lst = []
    for el in html_list.find_all("li"):
        lst.append(textify(el))
    return lst

def clean_table_key(cell) -> str:
    return cell.rstrip(":").strip()

def table_to_matrix(table: HtmlElement) -> List[List[HtmlElement]]:
    matrix = []
    for tr in table.findall("//tr"):
        tr: HtmlElement
        row = []
        for td in tr.cssselect("td,th"):
            row.append(td)
        matrix.append(row)
    return matrix


def table_to_dict(table: HtmlElement = None, matrix: List[List[str]] = None) -> Dict[str, str]:
    """Read a key-value style table to a dict"""
    if matrix is None:
        matrix = table_to_matrix(table)
    dic = {}
    for row in matrix:
        dic[clean_table_key(row[0])] = row[1]
    return dic


def table_to_dict_list(table: HtmlElement) -> List[Dict[str, str]]:
    """Read a table with headers into a list of rows"""
    return None