from itertools import cycle
from os import PathLike
from typing import Set, Dict

import requests
from requests import Response, Request


class InvalidResponse(Exception):
    def __init__(self, response, tries):
        super().__init__(f"Response invalid after {tries} proxies", response)


class BaseFetcher:
    ACCEPT_CODES: Set[int] = {200, }
    RETRIES: int = 3
    PROXY_FILE: PathLike = "proxy_list.txt"
    USER_AGENT: str = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0"
    DEFAULT_HEADERS: Dict[str, str] = {
        "User-Agent": USER_AGENT,
    }

    def __init__(self):
        # Load proxies
        with open(self.PROXY_FILE) as f:
            self.proxies = set((line.strip() for line in f.readlines()))
        self.proxy_rotator = cycle(self.proxies)

        # Prepare session
        self.session = requests.Session()
        self.set_proxy(next(self.proxy_rotator))
        self.session.headers = self.DEFAULT_HEADERS

    def is_valid(self, response: Response) -> bool:
        return response.status_code in self.ACCEPT_CODES

    def ensure_request(self, request: Request, **kwargs) -> Response:
        prepared_request = self.session.prepare_request(request)
        for i in range(self.RETRIES+1):
            response = self.session.send(prepared_request, **kwargs)
            if self.is_valid(response):
                return response
            self.set_proxy(next(self.proxy_rotator))
        raise InvalidResponse(response, self.RETRIES+1)

    def set_proxy(self, proxy):
        if proxy:
            self.session.proxies = {
                "HTTP": proxy,
                "HTTPS": proxy
            }
        else:
            self.session.proxies = None
