from unittest import TestCase

from slo_scrap_lib.util import pairwise


class TestUtil(TestCase):
    def test_pairwise1(self):
        input = [1,2,3,4,5,6]
        expected = [(1,2), (3,4), (5,6)]
        got = list(pairwise(input))
        self.assertListEqual(got, expected)
