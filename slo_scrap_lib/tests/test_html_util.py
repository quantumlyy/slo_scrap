import json
import unittest

from lxml import html as lhtml

from slo_scrap_lib.html_util import table_to_dict, table_to_matrix, html_list_to_list
from slo_scrap_lib.tests.test_utils import json_from_file, soup_from_file


class TestHtmlUtils(unittest.TestCase):

    def _test_table_to_dict(self, filename: str):
        matrix = json_from_file(f"test_data/{filename}_matrix.json")
        dict_got = table_to_dict(matrix=matrix)
        dict_expected = json_from_file(f"test_data/{filename}_dict.json")
        self.assertDictEqual(dict_got, dict_expected)

    def test_table_to_dict1(self):
        """Table→Dict with synthetic example"""
        self._test_table_to_dict("table1")

    def _test_table_to_matrix(self, filename: str):
        table = lhtml.parse(f"test_data/{filename}.html")
        matrix_got = table_to_matrix(table)
        matrix_expected = json_from_file(f"test_data/{filename}_matrix.json")
        self.assertListEqual(matrix_got, matrix_expected)

    def test_table_to_matrix(self):
        """Table→Dict with synthetic example"""
        self._test_table_to_matrix("table1")

    def _test_html_list_to_list(self, filename):
        table = soup_from_file(f"test_data/{filename}.html")
        list_got = html_list_to_list(table)
        list_expected = json_from_file(f"test_data/{filename}_list.json")
        self.assertListEqual(list_got, list_expected)

    def test_html_list_to_list1(self):
        self._test_html_list_to_list("unordered_list")
