from dataclasses import dataclass
from datetime import datetime
from typing import List
from warnings import warn

import lxml.html as lhtml
import requests
from pydantic import BaseModel
from requests import Request

from slo_scrap_lib.fetcher import BaseFetcher


class Fetcher(BaseFetcher):
    pass

FETCHER = Fetcher()


def seznam_postaj():
    url = "https://www.ap-ljubljana.si/_vozni_red/get_postajalisca_vsa_v2.php"
    resp = FETCHER.ensure_request(Request("GET", url))
    lines = resp.text.split("\n")
    splits = [line.split() for line in lines]
    pairs = [(split[0][2:], split[1]) for split in splits]
    return dict(pairs)

@dataclass
class Vožnja:
    timestamp: int
    from_id: int
    from_name: str
    to_id: int
    to_name: str
    odhod: str
    prihod: str
    dolžina: str


def vozni_red(vstop_id: int, izstop_id: int, slo_datum: str):
    url = "https://www.ap-ljubljana.si/_vozni_red/get_vozni_red_0.php"
    params = {
        "VSTOP_ID": vstop_id,
        "IZSTOP_ID": izstop_id,
        "DATUM": slo_datum
    }
    resp = FETCHER.ensure_request(Request("GET", url, params=params))
    vožnje = []

    for line in resp.iter_lines():
        split = line.split("|")
        v = Vožnja(*split[1:9])
        vožnje.append(v)
    return vožnje
