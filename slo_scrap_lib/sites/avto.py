from dataclasses import dataclass
from typing import Any, Dict, List

import lxml.html as lhtml
from bs4 import BeautifulSoup, PageElement
from requests import PreparedRequest, Request

from slo_scrap_lib.fetcher import BaseFetcher
from slo_scrap_lib.html_util import table_to_matrix, table_to_dict, textify, html_list_to_list
from slo_scrap_lib.util import flatten, pairwise


class Fetcher(BaseFetcher):
    DEFAULT_HEADERS = BaseFetcher.DEFAULT_HEADERS | {
        "Accept": "*/*",
    }

FETCHER = Fetcher()



def table_cells_to_dict(cells, clean_key=lambda s: textify(s).strip(":"), clean_value=textify):
    dic = {}
    for key_cell, val_cell in pairwise(cells):
        key = clean_key(key_cell)
        if not key: continue
        dic[key] = clean_value(val_cell)
    return dic


@dataclass
class Avto:
    osnovni_podatki: Dict[str, str]
    ostali_podatki: Dict[str, List[str]]
    cena: str
    opombe: str


def parse_single(url: str=None, id: int=None):
    if url is None:
        url = f"https://avto.net/Ads/details.asp?id={id}"

    resp = FETCHER.ensure_request(Request("GET", url))

    resp_content = resp.content.decode("windows-1250")
    content = string_from_to(resp_content, "-- TITLE --", "--  MODAL AD REPORT   --")

    #html = lhtml.fromstring(resp.text)
    html = BeautifulSoup(content)

    osnovni_podatki: Dict[str, str] = None
    ostali_podatki: Dict[str, List[str]] = None
    for table in html.find_all("table"):

        cells = table.find_all(["td", "th"])
        hdr = textify(cells[0])
        cells = cells[1:]
        if hdr == "Osnovni podatki":
            osnovni_podatki = table_cells_to_dict(cells)
        elif hdr == "Oprema in ostali podatki o ponudbi":
            ostali_podatki = table_cells_to_dict(cells, clean_value=html_list_to_list)

    opombe_content = string_from_to(content, "-- OPOMBE --", "INDIVIDUAL IFRAME")
    opombe_html = BeautifulSoup(opombe_content)
    opombe = opombe_html.find("div").get_text("\n").strip()

    cena_content = string_from_to(content, "-- AKCIJSKA CENA TXT--", "-- FINANCING --")
    cena_html = BeautifulSoup(cena_content)
    cena = cena_html.find("div").text.strip()

    return Avto(
        osnovni_podatki = osnovni_podatki,
        ostali_podatki = ostali_podatki,
        cena = cena,
        opombe = opombe,
    )


def string_from_to(content, match1=None, match2=None):
    start = 0
    end = len(content)
    if match1:
        start = content.find(match1)
    if match2:
        end = content.find(match2, start)
    return content[start:end]


if __name__ == '__main__':
    parse_single(id=16628747)
